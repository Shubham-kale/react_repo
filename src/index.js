import React from "react";
import ReactDom from "react-dom";

import "./index.css";

import {books} from "./books";  //No extension is needed for JS files, unlike for CSS
import MyBook from "./Book";

function BookList() {
  return (
    <React.Fragment>
      <section className="booklist">
       {
         books.map( (book) => {
           return (
            <MyBook key={book.id} book={book}/>
           );
         })
       }
      </section>
    </React.Fragment>
  );
}

ReactDom.render(<BookList />, document.getElementById("root"));
