export const books = [
  {
     id :1,
     title : "Word Power Made Easy",
     author : "Norman Lewis",
     img : "https://images-eu.ssl-images-amazon.com/images/I/91sJmZFOIQL._AC_UL200_SR200,200_.jpg"
  },
  {
    id : 2,
    title : "The Power of your Subconscious Mind",
    author : "Joseph Murphy",
    img : "https://images-eu.ssl-images-amazon.com/images/I/81gTwYAhU7L._AC_UL200_SR200,200_.jpg"
  }
] ;