import React from "react";

const Book = (props) => {
  const {title,author,img} = props.book;
  const clickHandler = (e) => {
     console.log(e);
     console.log(e.target);
     alert("Hello World!!");
  };
  const complexExample = (author) => {
    console.log(author);
  };
  return (
    <article className="book" onMouseOver={ () => {
      console.log(title);
    }}>
      <img
        src= {img}
        alt= ''
      />
      <h1 onClick={() => console.log(title)}>{title}</h1>
      <h4 onClick={() => complexExample(author)}>{author}</h4>
      <button type="button" onClick={clickHandler}>Buy Now</button>
    </article>
  );
};

export default Book;